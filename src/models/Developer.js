import React from "react";
import PropTypes from 'prop-types';
import api from "../services/api";

export const DeveloperContext = React.createContext({})

const Developer = {
    shape: PropTypes.PropTypes.shape({
        id: PropTypes.number.isRequired,
        nome: PropTypes.string.isRequired,
        idade: PropTypes.number.isRequired,
        hobby: PropTypes.string.isRequired,
        datanascimento: PropTypes.string.isRequired
    }),

    list(filters) {
        return api.get('developers', {
            params: filters
        });
    },

    create(subject) {
        return api.post('developers', subject);
    },

    findById(id) {
        return api.get(`developers/${id}`);
    },

    replace(id, subject) {
        return api.put(`developers/${id}`, {id, ...subject});
    },

    delete(id) {
        return api.delete(`developers/${id}`);
    }
}

export default Developer;