import CreateDeveloperFormContainer from '../../containers/CreateDeveloperFormContainer';

function Create() {
    return (
        <CreateDeveloperFormContainer />
    );
}

export default Create;