import DevelopersListContainer from "../../containers/DevelopersListContainer";

function Home() {
    return(
        <DevelopersListContainer />
    );
}

export default Home;