import UpdateDeveloperFormContainer from "../../containers/UpdateDeveloperFormContainer";
import {Redirect, useParams} from "react-router-dom";
import {useContext, useEffect, useState} from "react";
import Developer from "../../models/Developer";
import {HandlerContext} from "../../exceptions/Handler";

function Update() {
    const {report} = useContext(HandlerContext);
    const {id} = useParams();
    const [developer, setDeveloper] = useState(null);
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        Developer.findById(id)
            .then(response => {
                setDeveloper(response.data.data);
            })
            .catch(error => {
                report(error);
                setRedirect(true);
            })
    }, [id, report]);

    if (redirect) {
        return <Redirect to="/" />
    }

    return !developer ? <></> : <UpdateDeveloperFormContainer developer={developer}/>;
}

export default Update;