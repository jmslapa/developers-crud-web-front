import {useCallback, useContext, useState} from "react";
import DeveloperForm from "../components/DeveloperForm";
import Developer, {DeveloperContext} from "../models/Developer";
import {HandlerContext} from "../exceptions/Handler";
import {toast} from "react-toastify";
import {Redirect} from "react-router-dom";

function UpdateDeveloperFormContainer({developer}) {
    const {fetchDevelopers} = useContext(DeveloperContext);
    const {report} = useContext(HandlerContext);
    const [redirect, setRedirect] = useState(false);
    const [form, setForm] = useState({
        nome: developer.nome,
        hobby: developer.hobby,
        datanascimento: developer.datanascimento,
        sexo: developer.sexo
    });

    const onSubmit = useCallback((e) => {
        e.preventDefault();
        Developer.replace(developer.id, form)
            .then(() => {
                fetchDevelopers();
                toast.success('Developer updated with success.');
                setRedirect(true);
            })
            .catch(error => report(error))
    }, [developer, form, fetchDevelopers, report]);

    if (redirect) {
        return <Redirect to="/"/>
    }
    return <DeveloperForm form={form} setForm={setForm} onSubmit={onSubmit}/>;
}

UpdateDeveloperFormContainer.propTypes = {
    developer: Developer.shape.isRequired
}

export default UpdateDeveloperFormContainer;