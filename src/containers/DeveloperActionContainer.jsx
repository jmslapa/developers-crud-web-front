import ActionButtons from "../components/ActionButtons";
import Developer, {DeveloperContext} from "../models/Developer";
import {useCallback, useContext, useState} from "react";
import {toast} from "react-toastify";
import {HandlerContext} from "../exceptions/Handler";
import {Redirect} from "react-router-dom";

function DeveloperActionContainer({developer}) {

    const Developers = useContext(DeveloperContext);
    const Handler = useContext(HandlerContext);

    const [redirect, setRedirect] = useState(false);

    const onDelete = useCallback(() => {
        Developer.delete(developer.id)
            .then(() => {
                Developers.fetchDevelopers()
                toast.success('Developer was deleted.');
            })
            .catch(error => Handler.report(error))
    }, [Developers, Handler, developer]);

    const onUpdate = useCallback(() => {
        setRedirect(true);
    }, []);

    if (redirect) {
        return <Redirect to={`/update/${developer.id}`}/>
    }
    return (
        <ActionButtons className={"ml-auto"} onDelete={onDelete} onUpdate={onUpdate}/>
    );
}

DeveloperActionContainer.propTypes = {
    developer: Developer.shape.isRequired
}

export default DeveloperActionContainer;