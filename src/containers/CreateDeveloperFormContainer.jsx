import {useCallback, useContext, useState} from "react";
import moment from "moment";
import DeveloperForm from "../components/DeveloperForm";
import Developer, {DeveloperContext} from "../models/Developer";
import {HandlerContext} from "../exceptions/Handler";
import {toast} from "react-toastify";
import {Redirect} from "react-router-dom";

function CreateDeveloperFormContainer() {
    const {fetchDevelopers} = useContext(DeveloperContext);
    const {report} = useContext(HandlerContext);
    const [redirect, setRedirect] = useState(false);
    const [form, setForm] = useState({
        nome: '',
        hobby: '',
        datanascimento: moment().subtract(16, 'years').format('YYYY-MM-DD'),
        sexo: 'M'
    });

    const onSubmit = useCallback((e) => {
        e.preventDefault();
        Developer.create(form)
            .then(() => {
                fetchDevelopers();
                toast.success('Developer created with success.');
                setRedirect(true);
            })
            .catch(error => report(error))
    }, [form, fetchDevelopers, report]);

    if (redirect) {
        return <Redirect to="/"/>
    }
    return <DeveloperForm form={form} setForm={setForm} onSubmit={onSubmit}/>;
}

export default CreateDeveloperFormContainer;