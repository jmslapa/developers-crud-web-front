import DevelopersList from "../components/DevelopersList";
import {useContext, useEffect} from "react";
import {DeveloperContext} from "../models/Developer";

function DevelopersListContainer() {

    const {list, fetchDevelopers} = useContext(DeveloperContext);

    useEffect(() => {
        fetchDevelopers()
    }, [fetchDevelopers]);

    return (
        <DevelopersList data={list}/>
    );
}

export default DevelopersListContainer;