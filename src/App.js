import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Create from "./pages/Create";
import Update from "./pages/Update";
import Handler from "./exceptions/Handler";

function App() {


    return (
        <BrowserRouter>
            <ToastContainer/>
            <Handler>
                <Layout>
                    <Switch>
                        <Route component={Home} exact path="/"/>
                        <Route component={Create} exact path="/create"/>
                        <Route component={Update} exact path="/update/:id"/>
                        <Redirect to="/"/>
                    </Switch>
                </Layout>
            </Handler>
        </BrowserRouter>
    );
}

export default App;
