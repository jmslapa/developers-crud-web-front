import {Link} from "react-router-dom";

function TopBar() {

    return (
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <Link className="navbar-brand" to="/">Developers database</Link>
            <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavId">
                <ul className="navbar-nav mt-2 mt-lg-0 ml-auto">
                    <li className="nav-item active">
                        <Link type="Button" className="btn btn-success" to="/create">New</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default TopBar;