import PropTypes from 'prop-types';
import {useContext} from "react";
import {HandlerContext} from "../../exceptions/Handler";
import moment from "moment";

function DeveloperForm({form, setForm, onSubmit}) {

    const {errorBag, resetErrorBag} = useContext(HandlerContext)

    return (
        <div className="container">
            <form onSubmit={(e) => {
                resetErrorBag();
                onSubmit(e);
            }}>
                <fieldset className="form-group row">
                    <legend className="col-form-legend col-sm-1-12">Names</legend>
                    <div className="row col-12">
                        <input type="text" className="form-control" name="nome" id="nome"
                               placeholder="Developer's name"
                               value={form.nome}
                               onChange={(e) => setForm(state => ({...state, nome: e.target.value}))}/>
                        <div className={`invalid-feedback ${errorBag.nome ? 'd-block' : ''}`}>
                            {errorBag.nome}
                        </div>
                    </div>
                </fieldset>
                <fieldset className="form-group row">
                    <legend className="col-form-legend col-sm-1-12">Hobby</legend>
                    <div className="row col-12">
                        <input type="text" className="form-control" name="hobby" id="hobby"
                               placeholder="Developer's hobby"
                               value={form.hobby}
                               onChange={(e) => setForm(state => ({...state, hobby: e.target.value}))}/>
                        <div className={`invalid-feedback ${errorBag.hobby ? 'd-block' : ''}`}>
                            {errorBag.hobby}
                        </div>
                    </div>
                </fieldset>
                <div className="row col-12 justify-content-center">
                    <fieldset className="form-group row">
                        <legend className="col-form-legend col-sm-1-12">Gender</legend>
                        <div className="row col-12">
                            <div className="form-check mr-2">
                                <label className="form-check-label">
                                    <input type="radio" className="form-check-input" name="sexo" id="sexo_male"
                                           value="M"
                                           checked={form.sexo === 'M'}
                                           onChange={(e) => setForm(state => ({...state, sexo: e.target.value}))}/>
                                    Male
                                </label>
                            </div>
                            <div className="form-check">
                                <label className="form-check-label">
                                    <input type="radio" className="form-check-input" name="sexo" id="sexo_female"
                                           value="F"
                                           checked={form.sexo === 'F'}
                                           onChange={(e) => setForm(state => ({...state, sexo: e.target.value}))}/>
                                    Female
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset className="form-group row mr-5 align-items-center ml-5">
                        <legend className="col-form-legend col-sm-1-12">Birth date</legend>
                        <div className="col-sm-1-12">
                            <input type="date" className="form-control" name="datanascimento" id="datanascimento"
                                   placeholder="Developer's birth date"
                                   max={moment().subtract(16, 'years').format('YYYY-MM-DD')}
                                   value={form.datanascimento}
                                   onChange={(e) => setForm(state => ({...state, datanascimento: e.target.value}))}/>
                        </div>
                    </fieldset>
                </div>
                <div className="form-group row col-12 p-0 justify-content-center mt-5">
                    <button type="submit" className="btn btn-success w-100">Submit</button>
                </div>
            </form>
        </div>
    );
}

DeveloperForm.propTypes = {
    form: PropTypes.shape({
        nome: PropTypes.string,
        hobby: PropTypes.string,
        datanascimento: PropTypes.string,
        sexo: PropTypes.string
    }).isRequired,
    setForm: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}

export default DeveloperForm;