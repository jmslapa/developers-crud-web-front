import {Eye} from 'styled-icons/fa-regular'
import {Button} from "./styles";

function ShowBtn({onClick}) {
    return (
        <Button type="button" className="btn btn-outline-info  btn-sm" onClick={onClick}>
            <Eye size={25}/>
        </Button>
    );
};

export default ShowBtn;