import {Trash} from 'styled-icons/boxicons-regular'
import {Button} from "./styles";


function DeleteBtn({onClick}) {
    return (
        <Button type="button" className="btn btn-outline-danger btn-sm" onClick={onClick}>
            <Trash size={25}/>
        </Button>
    );
};

export default DeleteBtn;