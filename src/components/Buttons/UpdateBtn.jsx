import {Edit} from 'styled-icons/boxicons-regular'
import {Button} from "./styles";


function UpdateBtn({onClick}) {
    return (
        <Button type="button" className="btn btn-outline-secondary  btn-sm" onClick={onClick}>
            <Edit size={25}/>
        </Button>
    );
};

export default UpdateBtn;