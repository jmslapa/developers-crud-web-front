import PropTypes from 'prop-types'
import ShowBtn from "../Buttons/ShowBtn";
import UpdateBtn from "../Buttons/UpdateBtn";
import DeleteBtn from "../Buttons/DeleteBtn";

function ActionButtons({ onShow, onUpdate, onDelete }) {

    return (
        <div className="row">
            {onShow ? <ShowBtn onClick={onShow}/> : false}
            {onUpdate ? <UpdateBtn onClick={onUpdate}/> : false}
            {onDelete ? <DeleteBtn onClick={onDelete}/> : false}
        </div>
    );
}

ActionButtons.propTypes = {
    onShow: PropTypes.func,
    onUpdate: PropTypes.func,
    onDelete: PropTypes.func
};

export default ActionButtons;