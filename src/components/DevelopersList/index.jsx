import PropTypes from 'prop-types';
import Developer from "../../models/Developer";
import DevelopersListItem from "./DevelopersListItem";

function DevelopersList({ data }) {
    return (
        <ul className="list-group">
            {data.map((
                developer => <DevelopersListItem developer={developer} key={`{developer_${developer.id}`} />
            ))}
        </ul>
    );
}

DevelopersList.propTypes = {
    data: PropTypes.arrayOf(Developer.shape)
};

export default DevelopersList;