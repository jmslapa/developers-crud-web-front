import Developer from "../../models/Developer";
import DeveloperActionContainer from "../../containers/DeveloperActionContainer";

function DevelopersListItem({ developer }) {

    return (
        <li className="list-group-item d-flex justify-content-between align-items-center">
            <div className="h5">{`${developer.nome}, ${developer.idade} anos, gosta de ${developer.hobby.toLowerCase()}`}</div>
            <DeveloperActionContainer developer={developer}/>
        </li>
    );
}

DevelopersListItem.propTypes = {
    developer: Developer.shape.isRequired
};

export default DevelopersListItem;