import TopBar from "../TopBar";
import PropTypes from "prop-types";
import {HandlerContext} from "../../exceptions/Handler";
import Developer, {DeveloperContext} from "../../models/Developer";
import {useCallback, useContext, useState} from "react";

function Layout({children}) {

    const {report} = useContext(HandlerContext);
    const [list, setList] = useState([]);

    const fetchDevelopers = useCallback(() => {
        Developer.list()
            .then((resp) => setList(resp.data.data))
            .catch(error => {
                setList([]);
                report(error)
            });
    }, [report]);

    return (
        <DeveloperContext.Provider value={{list, fetchDevelopers}}>
            <div className="container">
                <header className="mb-5">
                    <TopBar/>
                </header>
                {children}
            </div>
        </DeveloperContext.Provider>
    );
}

Layout.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Layout