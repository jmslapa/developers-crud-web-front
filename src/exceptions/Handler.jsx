import React, {useCallback, useEffect, useState} from "react";
import PropTypes from 'prop-types';
import {toast} from "react-toastify";

export const HandlerContext = React.createContext({});

function Handler({children}) {

    const [error, setError] = useState({});
    const [errorBag, setErrorBag] = useState({});

    const report = useCallback((error) => setError(error), []);
    const resetErrorBag = useCallback(() => setErrorBag({}), []);

    const handle = useCallback((error) => {
        switch (error.response.status) {
            case 400:
                setErrorBag(error.response.data.errors);
                break;
            default:
                break;
        }
        toast.error(error.response.data.message);
    }, []);

    useEffect(() => {
        if (error.response) {
            handle(error);
        }
    }, [error, handle]);

    return (
        <HandlerContext.Provider value={{errorBag, resetErrorBag, report}}>
            {children}
        </HandlerContext.Provider>
    );
}

Handler.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Handler;